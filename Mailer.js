const mail = require('@sendgrid/mail')
const format = require('util').format
const _ = require('lodash')
const config = require('./config')

mail.setApiKey(config.sendgridApiKey)

const requiredOptions = {
	to: config.mailTo,
	from: config.mailFrom,
	subject: config.mailSubject,
	text: null,
	html: null
}

function sendMail (message, subject) {
	let body = typeof message === 'string' ? message : JSON.stringify(message, null, 4)
	let options = {
		text: body,
		html: format('<pre>%s</pre>', body)
	}

	if (subject) options.subject = subject

	return mail.send(_.extend({}, requiredOptions, options))
}

module.exports = sendMail
