const assert = require('assert')
const _ = require('lodash')
const Ingester = require('../../Ingester')

class feedIngester extends Ingester {
    initialize(){

    }
}

describe('class Ingester', function(){
    var ingester = new feedIngester()

    var calculateCondition = function(func, text, condition){
        //console.log(arguments)
        text.trim().split(',').forEach(function(k){
            // console.info(k.trim(), '->', condition)
            return assert.strictEqual(
                condition||null,
                func(k),
                k + ' could not converted to ' + condition
            )
        })
    }

    it('validatePrice', function(){
        [
            [0, '0.00'],
            [1, '1.00'],
            [-1, '-1.00'],
            ['', null],
            ['1.126', '1.13'],
            ['abc', null],
            ['12.abc', '12.00'],
            ['xyz23', null]
        ].forEach(function(v){
            assert.strictEqual(v[1], ingester.validatePrice(v[0]))
        })
    })

    it('validateGender', function(){
        _.each({
            'Mens': 'Men, Man, bOY, male',
            'Womens': 'Women, Woman, Lady, Ladies, Girls, female',
            '': 'bogus'
        }, calculateCondition.bind(this, ingester.validateGender))
    })

    it('validateCondition', function(){
        _.each({
            'Never Worn': 'new, never worn, unused',
            'Pre-Owned': 'owned, like new, used',
            '': 'bogus'
        }, calculateCondition.bind(this, ingester.validateCondition))
    })

    it('validateCategories', function(){
        _.each({
            'Watches': 'watch, watches',
            'Jewelry': 'jewelry',
            'Jewelry/Bracelets': 'jewelry bracelet',
            'Jewelry/Brooches': 'jewelry brooch',
            'Jewelry/Earrings': 'jewelry earring',
            'Jewelry/Cufflinks': 'jewelry cufflink',
            'Jewelry/Necklaces': 'jewelry necklace',
            'Jewelry/Pendants': 'jewelry pendant',
            'Jewelry/Rings': 'jewelry ring',
            '': 'bogus'
        }, calculateCondition.bind(this, ingester.validateCategories))
    })

    it('validateAttributeSet', function(){
        _.each({
            'Watches': 'watch',
            'Jewelry': 'jewelry,bracelet,brooch,earring,cufflink,necklace,pendant,ring',
            '': 'bogus'
        }, calculateCondition.bind(this, ingester.validateAttributeSet))
    })

    it('validateMovement', function(){
        _.each({
            'Manual': ' manual ',
            'Quartz': ' quartz ',
            'Automatic': ' automatic ',
            '': 'bogus'
        }, calculateCondition.bind(this, ingester.validateMovement))
    })

    it('validateQty', function(){
        _.each([
            ['0', 0],
            ['1', 1],
            [null, 0],
            ['a1', 0],
            ['1a', 1],
            [NaN, 0],
            [undefined, 0]
        ], function(v){
            assert.strictEqual(v[1], ingester.validateQty(v[0]))
        })
    })


})
