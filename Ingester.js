const request = require('request')
const FTP = require('ftp')
const fs = require('fs')
const xml2js = require('xml2js')
const xmlParser = new xml2js.Parser()
const csvParser = require('csv-parse')
const csvWriter = require('csv-write-stream')
const urlParse = require('url')
const crypto = require('crypto')
const shell = require('shelljs')
const exec = require('child_process').exec
const _ = require('lodash')
const pluralize = require('pluralize')
const htmlToText = require('html-to-text')
const mailer = require('./Mailer')
const debug = require('debug')('ingester')

const defaultCSVRow = {
	sku: null,
	condition: null,
	name: null,
	manufacturer: null,
	price: null,
	cost: null,
	special_price: null,
	simples_skus: null,
	configurable_attributes: null,
	lux_ring_size: null,
	description: null,
	gender: null,
	metal_type: null,
	movement: null,
	weight: null,
	stone_type: null,
	stoneCarat: null,
	categories: null,
	image: null,
	small_image: null,
	thumbnail: null,
	gallery: null,
	store: 'default,admin',
	websites: 'base',
	status: 2,
	type: 'simple',
	visibility: 4,
	tax_class_id: 'none',
	qty: 1,
	is_in_stock: 1,
	attribute_set: null,
	partner: null,
	vendor_sku: null,
	meta_title: null,
	third_party_url: null
}
const defaultCSVParseOptions = {
	delimiter: ',',
	relax: true,
	auto_parse: true,
	columns: true,
	quote: '"',
	rowDelimiter: '\n',
	skip_empty_lines: true
}

class Ingester {
	constructor (options) {
		this.log = []
		this.importFolder = '/var/www/truefacets/var/import/'
		this.csvBase = this.importFolder + 'scrapper/'
		this.imageBase = this.importFolder + 'images/'
		this.options = _.extend({debug: false, useRawFile: false}, options)
		debug.enabled = debug.enabled || Boolean(this.options.debug)
		this.csvParseOptions = _.extend({}, defaultCSVParseOptions)
		this.defaults = _.extend({}, defaultCSVRow)

		this.map = _.get(options, 'map', {})
		this.analize = {
			source: _.get(options, 'sourceAnalize', []),
			result: _.get(options, 'resultAnalize', ['categories', 'attribute_set', 'condition', 'gender', 'manufacturer'])
		}

		this.source = _.get(options, 'source', '')
		this.feedName = this.constructor.name

		this.initialize(options)

		let url = urlParse.parse(this.source)
		this.feedHash = this.md5Partial(url.hostname || this.siteName || this.feedName)

		this.basefile = this.csvBase + this.constructor.name + (debug.enabled ? '' : '-' + Date.now())
		if (this.options.useRawFile && fs.existsSync(this.basefile + '.raw')) {
			this.source = 'file:' + this.basefile + '.raw'
		}

		shell.mkdir('-p', this.importFolder)
		shell.mkdir('-p', this.csvBase)
		shell.mkdir('-p', this.imageBase)

		this.csvFile = this.basefile + '.csv'

		this.log.push('Feed: ' + this.constructor.name)
		this.log.push('Source: ' + this.source)
		this.log.push('CSV file: ' + this.csvFile)

		debug('source:', this.source)
		debug('destination:', this.basefile + '.csv')
	}

	initialize () {}

	static get description () {
		return ''
	}

	validateSku (value) {
		let sku = String(value || '').replace(/[^a-z0-9-_]/ig, '-')
		return sku ? [sku, this.feedHash].join('-') : null
	}

	validatePrice (value) {
		value = parseFloat(value).toFixed(2)
		return isNaN(value) ? null : value
	}

	validateCost (value) {
		return this.validatePrice(value)
	}

	validateSpecialPrice (value) {
		return this.validatePrice(value)
	}

	validateGender (value) {
		if (/women|woman|lady|ladies|girl|female/i.test(value)) {
			return 'Womens'
		}
		if (/men|man|boy|male/i.test(value)) {
			return 'Mens'
		}
		return null
	}

	validateCondition (value) {
		let condition = String(value || '').toLowerCase().replace(/[^0-9a-z]/gi, '')

		let stat = 0 // 0: unknown
		let NEW = 1
		let USED = 2

		stat = stat || (/neverworn|unused/.test(condition) && NEW)
		stat = stat || (/owned|likenew|used/.test(condition) && USED)
		stat = stat || (/new/.test(condition) && NEW)

		return stat === USED ? 'Pre-Owned' : stat ? 'Never Worn' : null
	}

	validateCategories (value) {
		value = String(value)
		if (/watch/i.test(value)) {
			return 'Watches'
		}

		var re = /(bracelet|brooch|earring|cufflink|necklace|pendant|ring)/i
		let matches = value.match(re)
		if (matches) return 'Jewelry/' + _.capitalize(pluralize.plural(matches[1]))

		return /jewelry/i.test(value) ? 'Jewelry' : null
	}

	validateAttributeSet (value) {
		let isWatch = /watch/i.test(value)
		let isJewelry = /(jewelry|bracelet|brooch|earring|cufflink|necklace|pendant|ring)/i.test(value)
		return isWatch ? 'Watches' : isJewelry ? 'Jewelry' : null
	}

	validateMovement (value) {
		switch (String(value || '').toLowerCase().replace(/[^0-9a-z]/g, '')) {
		case 'manual':
			return 'Manual'
		case 'quartz':
			return 'Quartz'
		case 'automatic':
			return 'Automatic'
		default:
			return null
		}
	}

	validateMetaTitle (value, item) {
		return value ? String(value) + ' | Buy at TrueFacet' : null
	}

	validateDescription (value, item) {
		return htmlToText.fromString(value)
	}

	getImages (value, item) {
		return String(value).split(/[\s,;]+/).filter(item => item).map(url => url.trim())
	}

	validateGallery (value, item) {
		let images = this.getImages(value, item)
		return images.length ? '+' + images.join(';+') : ''
	}

	validateImage (value, item) {
		let images = this.getImages(value, item)
		return images.length ? '+' + images[0] : ''
	}

	validateSmallImage (value, item) {
		return this.validateImage(value, item)
	}

	validateThumbnail (value, item) {
		return this.validateImage(value, item)
	}

	validateQty (value, item) {
		return parseInt(value) || 0
	}

	processItem (item) {
		var data = _.extend({}, this.defaults)
		_.forIn(this.map, (lookup, key) => {
			let method = _.camelCase(['validate', key].join('-'))
			if (typeof lookup === 'function') {
				data[key] = lookup(item)
			} else if (typeof this[method] === 'function') {
				data[key] = this[method](item[lookup], item)
			} else {
				data[key] = item[lookup] || null
			}
		})
		return data
	}

	processData (items) {
		this.skuPriceMap = {}
		var jobs = items.map(item => {
			return this.processItem(item)
		})
		return Promise.all(jobs).then(result => {
			var data = result.filter(item => item !== null)
			var writer = csvWriter()
			writer.pipe(fs.createWriteStream(this.csvFile))
			_.each(data, item => {
				if (item) {
					writer.write(item)
					this.skuPriceMap[item.sku] = item.special_price || item.price
				}
			})
			writer.end()
			return data
		})
	}

	parseXMLData (data) {
		return new Promise((resolve, reject) => {
			xmlParser.parseString(data, function (err, result) {
				if (err) return reject(err)
				resolve(result)
			})
		})
	}

	parseCSVData (data) {
		return new Promise((resolve, reject) => {
			csvParser(String(data).replace(/[\r\n]/g, '\n'), this.csvParseOptions, (err, items) => {
				if (err) return reject(err)
				resolve(items)
			})
		})
	}

	parseRawData (data) {
		return data.indexOf('<?xml') === 0 ? this.parseXMLData(data) : this.parseCSVData(data)
	}

	getRawData () {
		return new Promise((resolve, reject) => {
			let url = urlParse.parse(this.source)
			switch (url.protocol) {
			case 'http:':
			case 'https:':
				request(this.source, (err, res, body) => {
					if (err) return reject(err)
					let status = res && res.statusCode
					if (status !== 200) {
						let msg = 'response status code <> 200, ' + status + '\n\n' + htmlToText.fromString(body)
						return reject(new Error(msg))
					}
					resolve(body)
				})
				break
			case 'ftps:':
				let url = urlParse.parse(this.source)
				var ftp = new FTP()
				var data = []
				ftp.on('ready', function () {
					ftp.get(url.path, function (err, stream) {
						if (err) return reject(err)
						stream.once('close', () => {
							ftp.end()
							resolve(Buffer.concat(data))
						})
						stream.on('data', chunk => data.push(chunk))
					})
				})
				ftp.on('error', (err) => {
					reject(err)
				})
				ftp.connect({
					secure: true,
					host: url.host,
					user: url.auth.split(':')[0],
					password: url.auth.split(':')[1],
					secureOptions: {rejectUnauthorized: false}
				})
				break
			case 'file:':
				let filename = urlParse.parse(this.source).pathname
				fs.readFile(filename, 'utf8', (err, data) => {
					if (err) return reject(err)
					resolve(data)
				})
				break
			default:
				reject(new Error('source url is not in acceptable protocols'))
			}
		})
		.then(rawdata => {
			if(!rawdata.length) throw new Error('Feed is empty')
			return rawdata
		})
	}

	isNotIgnorableError (error) {
		var re = /^(PHP Warning|PHP Notice|Command failed|PHP Parse error|Parse error)/
		error && debug('Error on Shell Exec:', error)
		return error && !re.test(error.trim())
	}

	execMagmiImport () {
		return new Promise((resolve, reject) => {
			var cmd = 'php /var/www/truefacets/magmi/cli/magmi.cli.php -profile=scrapper_product_insert_update -mode=xcreate -CSV:filename="' + this.csvFile + '"'
			debug('magmi command:', cmd)
			exec(cmd, (err, stdout, stderr) => {
				if (
					this.isNotIgnorableError(err ? err.message : '') ||
					this.isNotIgnorableError(stderr) ||
					this.isNotIgnorableError(stdout)
				) {
					return reject(new Error([stderr, stdout].join('\n')))
				}
				resolve(true)
			})
		})
	}

	execCustomerPartnerAssoc () {
		return new Promise((resolve, reject) => {
			var jsonSkusFile = this.basefile + '-skus.json'
			fs.writeFileSync(jsonSkusFile, JSON.stringify(this.skuPriceMap))
			var cmd = 'php /var/www/truefacets/shell/customer_partner_assoc.php --vendor "' + this.vendorEmail + '" --skus "' + jsonSkusFile + '"'
			debug('assoc command:', cmd)
			exec(cmd, (err, stdout, stderr) => {
				if (
					this.isNotIgnorableError(err ? err.message : '') ||
					this.isNotIgnorableError(stderr) ||
					this.isNotIgnorableError(stdout)
				) {
					return reject(new Error([stderr, stdout].join('\n')))
				}
				resolve(true)
			})
		})
	}

	runImportScripts () {
		return this
			.execMagmiImport()
			.then(this.execCustomerPartnerAssoc.bind(this))
	}

	sendAnalizeReport () {
		if (debug.enabled) return
		let logs = this.log.map(log => {
			return typeof log === 'string' ? log : JSON.stringify(log, null, 4)
		})
		return mailer(logs.join('\n'), '(SUCCESS) Ingester logs - ' + this.feedName)
			.then(() => {
				return 0
			})
	}

	sendErrorMail (err) {
		if (debug.enabled) throw err
		let message = [
			'Feed: ' + this.feedName,
			'Source: ' + this.source,
			'Error: ' + err.message
		].join('\n')

		return mailer(message, '(ERROR) Ingester logs - ' + this.feedName)
			.then(() => {
				throw err
			})
	}

	processFeed () {
		return this
			.getRawData()
			.then(this.saveSourceData.bind(this))
			.then(this.parseRawData.bind(this))
			.then(this.analizeSourceData.bind(this))
			.then(this.processData.bind(this))
			.then(this.analizeResultData.bind(this))
			.then(this.runImportScripts.bind(this))
			// .then(this.sendAnalizeReport.bind(this))
			.catch(this.sendErrorMail.bind(this))
	}

	printAnalizeData (data, title) {
		this.log.push(title + ': ' + JSON.stringify(data, null, 4))
		debug(title + ':', JSON.stringify(data, null, 4))
	}

	analizeData (items, fields) {
		var counter = {}
		fields.forEach(key => { counter[key] = {} })
		items.forEach(item => {
			fields.forEach(field => {
				let value = String(item[field])
				let n = counter[field][value] || 0
				counter[field][value] = n + 1
			})
		})
		return counter
	}

	saveSourceData (result) {
		if (this.protocol === 'file:') return result
		if (!this.options.useRawFile) return result
		let rawFile = this.basefile + '.raw'
		fs.writeFileSync(rawFile, result)
		return result
	}

	md5Partial (data) {
		return crypto.createHash('md5').update(String(data)).digest('hex').slice(0, 5).toUpperCase()
	}

	analizeSourceData (items) {
		if (!debug.enabled) return items
		let columns = _.keys(_.first(items))
		debug('columns:', columns.join(', '))
		debug('first record from source:', items[0])
		let data = this.analizeData(items, this.analize.source)
		this.printAnalizeData(data, 'Source Data Analize')
		return items
	}

	analizeResultData (items) {
		if (!debug.enabled) return items
		debug('first record on generated csv file:', items[0])
		let data = this.analizeData(items, this.analize.result)
		this.printAnalizeData(data, 'Result Data Analize')
		return items
	}
}

module.exports = Ingester
