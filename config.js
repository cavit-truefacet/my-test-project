module.exports = {
	mailTo: process.env.MAIL_TO || 'me@company.com',
	mailFrom: 'me@company.com',
	mailSubject: 'Feed Ingester Log',
	sendgridApiKey: 'top-secret-key'
}
